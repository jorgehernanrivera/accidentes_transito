FROM node:carbon
LABEL name "Aplicacion Docker sobre accidentes de transito"
MAINTAINER Johana León - Jorge Rivera
WORKDIR /app
ADD . /app
RUN npm install
RUN npm install -g nodemon
RUN ls -al /app
EXPOSE 80
CMD ["nodemon", "index.js"]
### CMD ["npm", "start"]
