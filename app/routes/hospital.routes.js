module.exports = (app) => {
    const hospital = require('../controllers/hospital.controller.js');
    // Crea un nuevo hospital
    app.post('/hospital', hospital.create);
    // Consulta todos los hospitales
    app.get('/hospital', hospital.findAll);
    // Consulta un hospital por id
    app.get('/hospital/:id', hospital.findOne);
    // Consulta los hospitales más cercanos
    app.get('/hospital/nearest/accident/:id/:distance', hospital.findNearest);
    // Elimina un hospital
    app.delete('/hospital/:id', hospital.delete);
   }