module.exports = (app) => {
  const accident = require('../controllers/accident.controller.js');
  // Crea un nuevo accidente
  app.post('/accident', accident.create);
  // Consulta todos los accidentes
  app.get('/accident', accident.findAll);
  // Consulta un accidente por Id
  app.get('/accident/:id', accident.findOne);
  // Consulta los 10 accidentes activos más recientes
  app.get('/accidentrecent', accident.findRecent);
  // Actualiza el estado de un accidente
  app.put('/accident/:id', accident.updateState);
}