const mongoose = require('mongoose');
const AccidentSchema = mongoose.Schema({
    location: {
        type: { type: String },
        coordinates: [],
    },
    photo: {
        data: Buffer,
        contentType: String
    },
    type: {
        type: String,
        enum: ['Choque', 'Atropellamiento', 'Volcamiento', 'Caída de ocupante', 'Incendio', 'Otros']
    },
    priority: {
        type: String,
        enum: ['Alta', 'Media', 'Baja']
    },
    date: Date,    
    state: {
        type: String,
        enum: ['Manejado', 'Pendiente']
    },
    hospital_name: String
}, {
        timestamps: true
    });
module.exports = mongoose.model('Accident', AccidentSchema);
