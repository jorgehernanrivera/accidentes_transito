const mongoose = require('mongoose');
const HospitalSchema = mongoose.Schema({
    location: {
        type: { type: String },
        coordinates: [],
    },
    name: String,
    priorities: []
    }, {
    timestamps: true
});
module.exports = mongoose.model('Hospital', HospitalSchema);