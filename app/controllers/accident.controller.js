const Accident = require('../models/accident.model.js');
// Modulo para subir ficheros
//const fs = require('fs');

// Crea un nuevo accidente
exports.create = (req, res) => {
    if (Object.keys(req.body).length === 0) {
        return res.status(400).send({
            message: "El accidente no puede guardarse vacío"
        });
    }
    // Crear un accidente con los datos ingresados por el usuario
    const accident = new Accident({
        location: {
            type: "Point",
            coordinates: [parseFloat(req.body.latitude),parseFloat(req.body.longitude)]
        },
        photo: {
            data: req.files.photo.data,
            contentType: req.files.photo.mimetype
        },
        type: req.body.type,
        priority: req.body.priority,
        date: new Date(),
        state: "Pendiente",
        hospital_name: ""
    });
    // Guarda el producto en la base de datos
    accident.save()
        .then(data => {
            res.status(200).send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Ocurrió un error mientras se guardaba el accidente."
            });
        });
};

// Lista todos los accidentes
exports.findAll = (req, res) => {
    Accident.find()
        .then(accident => {
            res.status(200).send(accident);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Ocurrió un error mientras se consultaban los registros."
            });
        });
};

// Obtiene un accidente por Id
exports.findOne = (req, res) => {
    Accident.findById(req.params.id)
        .then(accident => {
            if (!accident) {
                return res.status(404).send({
                    message: "No se encontró accidente con id: " + req.params.id
                });
            }
            res.status(200).send(accident);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "No se encontró accidente con id:" + req.params.id
                });
            }
            return res.status(500).send({
                message: "Ocurrió un error mientras se consultaba accidente con id:"
                    + req.params.id
            });
        });
};

// Consulta los últimos 10 accidentes activos reportados (el mas reciente primero)
exports.findRecent = (req, res) => {
    Accident.find({state: "Pendiente"}).
        limit(10).
        sort({date:-1})
        .then(accident => {
            res.status(200).send(accident);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Ocurrió un error mientras se consultaban los registros."
            });
        });
};

// Actualiza el estado de un accidente por Id
exports.updateState = (req, res) => {
    if (Object.keys(req.body).length === 0) {
        return res.status(400).send({
            message: "El accidente no puede estar vacio"
        });
    }
    // Encuentra el accidente y lo actualiza
    Accident.findByIdAndUpdate(req.params.id, {
        state: 'Manejado',
        hospital_name: req.body.hospital_name
    }, { new: true })
        .then(accident => {
            if (!accident) {
                return res.status(404).send({
                    message: "No se encontró accidente con id: " + req.params.id
                });
            }
            res.status(200).send(accident);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "No se encontró accidente con id: " + req.params.id
                });
            }
            return res.status(500).send({
                message: "Ocurrió un error mientras se actualizaba accidente con id: " +
                    req.params.id
            });
        });
};

        