const Hospital = require('../models/hospital.model.js');
const Accident = require('../models/accident.model.js');
 // Crea un nuevo accidente

 exports.create = (req, res) => {
    if (Object.keys(req.body).length === 0) {
        return res.status(400).send({
            message: "El hospital no puede guardarse vacío"
        });
    }
    
    // Crear un accidente con los datos ingresados por el usuario
    const hospital = new Hospital({
        location: req.body.location,
        name: req.body.name,
        priorities: req.body.priorities
    });
    // Guarda el producto en la base de datos
    hospital.save()
        .then(data => {
            res.status(200).send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Ocurrió un error mientras se guardaba el hospital."
            });
        });
};

// Lista todos los hospitales
exports.findAll = (req, res) => {
    Hospital.find()
        .then(hospital => {
            res.status(200).send(hospital);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Ocurrió un error mientras se consultaban los registros."
            });
        });
};

// Obtiene un hospital por Id
exports.findOne = (req, res) => {
    Hospital.findById(req.params.id)
        .then(hospital => {
            if (!hospital) {
                return res.status(404).send({
                    message: "No se encontró hospital con id: " + req.params.id
                });
            }
            res.status(200).send(hospital);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "No se encontró hospital con id:" + req.params.id
                });
            }
            return res.status(500).send({
                message: "Ocurrió un error mientras se consultaba hospital con id:"
                    + req.params.id
            });
        });
};


// Consulta los hospitales más cercanos dentro del radio ingresado por el usuario
exports.findNearest = (req, res) => {
    console.log(req.query);
    Hospital.ensureIndexes({location:"2dsphere"});

    //Se busca el accident
    Accident.findById(req.params.id)
        .then(accident => {
            if (!accident) {
                return res.status(404).send({
                    message: "No se encontró accidente con id: " + req.params.id
                });
            }
            //Se buscan los hospitales cercanos
            Hospital.find({
                'location': {
                    $near: {
                        $geometry: {
                           type : "Point",
                           coordinates : accident.location.coordinates
                        },
                        $maxDistance: req.params.distance
                     }
                },
                'priorities': accident.priority
            }).then(hospital => {
                    res.status(200).send(hospital);
                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Ocurrió un error mientras se consultaban los registros."
                    });
                });
            //res.status(200).send(accident);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "No se encontró accidente con id:" + req.params.id
                });
            }
            return res.status(500).send({
                message: "Ocurrió un error mientras se consultaba accidente con id:"
                    + req.params.id
            });
        });    
};
// Elimina un hospital
exports.delete = (req, res) => {
    Hospital.findByIdAndRemove(req.params.id)
    .then(hospital => {
        if(!hospital) {
            return res.status(404).send({
            message: "Hospital no encontrado con id:" + req.params.id
            });
        }
        res.status(200).send({message: "Hospital eliminado!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
            message: "Hospital no encontrado con id:" + req.params.id
            });
        }
        return res.status(500).send({
            message: "Algo malo ocurrió mientras se eliminaba el hospital con id:" +
            req.params.id
        });
    });
   };
