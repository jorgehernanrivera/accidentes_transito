// Import the required modules [1]
////////////////////////////////////////////////////////////////////////
const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');
// Database configuration
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');
mongoose.Promise = global.Promise;

// Create the Express Application [2]
////////////////////////////////////////////////////////////////////////
const app = express();
// Configure the server [3]
////////////////////////////////////////////////////////////////////////
// Parse requests of content-type - "application/x-www-form-urlencoded"
app.use(bodyParser.urlencoded({ extended: true }))
// Parse requests of content-type - "application/json"
app.use(bodyParser.json())
// Activate the CORS access on all routes
app.use(cors())
app.use(fileUpload())
// Listening server port
var port = process.env.PORT || 80;
// Define the routes [4]
////////////////////////////////////////////////////////////////////////
app.get('/', (req, res) => {
    res.json({
        "message": "This is a JSON response to a HTTP GET request."
    });
});

require('./app/routes/accident.routes.js')(app);
require('./app/routes/hospital.routes.js')(app);
// Start the server with selected configuration [5]
////////////////////////////////////////////////////////////////////////
app.listen(port, () => {
    console.log("El servidor está escuchando en el puerto: " + port);
});
// Connect to the database
mongoose.connect(dbConfig.url)
    .then(() => {
        console.log("Conectado a la base de datos ...");
    }).catch(err => {
        console.log('Falló la conexión a la base de datos');
        process.exit();
    });

